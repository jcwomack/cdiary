# Basic Makefile template for a C program
# James Womack 2013
CC = gcc
PROFILE = 
LDFLAGS = $(PROFILE)
CFLAGS = -c -Wall
INCLUDES =
MAIN = cdiary
DIRSRC = src
OBJ := $(patsubst %.c,%.o,$(wildcard $(DIRSRC)/*.c))

.PHONY: clean test

$(MAIN) : $(OBJ)
	$(CC) $(LDFLAGS) $(OBJ) $(INCLUDES) -o $(MAIN)

%.o : %.c
	$(CC) $(CFLAGS) $< -o $@

clean :
	rm -rf $(MAIN) $(OBJ)

test :
