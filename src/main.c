// Utility for generating template files with correct dates for weekly
// work diary.
// Work diary always starts on a Monday. 
// The starting date and destination directory can be defined on the command 
// line, otherwise they default to the next Monday and the current directory.
#define VERSION 0.1
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<unistd.h>
#include<string.h>
#include"setstart.h"

// strptime prototype required for clean compile
char *strptime(const char *, const char *, struct tm *);

int main(int argc, char* argv[]) {
  time_t rawtime;
  struct tm * pnow;
  struct tm * pstart = (struct tm *) malloc(sizeof(*pstart));
  unsigned int ii; // counter
  unsigned int option;
  unsigned int time_set = 0; // 1 if set via command line, 0 if not
  unsigned int dir_set = 0;
  unsigned int format_set = 0;
  char *time_string;
  char *dir_string;
  char *format_string;
  char *filename; // YYYYMMDD.txt
  char *filepath;
  char *output_time;
  size_t max_output_size;
  FILE *file;

  printf("cdiary: quick and dirty diary template generation, v%f.\n",VERSION);

  // If command line argument present, use as starting date.
  // Check for command line argument specifying directory to store diary file
  // in.
  // -t [ start date for diary ] 
  // -d [ directory for diary ]
  while( (option = getopt(argc, argv, "t:d:f:") ) != -1 ) {
     switch(option) {
       case 't':
//       printf("Option -t present, specifying time %s.\n", optarg);
         time_set = 1;
         time_string = optarg;
         break;
       case 'd':
//       printf("Option -d present, specifying directory %s.\n", optarg);
         dir_set = 1;
         dir_string = optarg;
         break;
       case 'f':
//       printf("Option -f present, specifying format %s.\n", optarg);
         format_set = 1;
         format_string = optarg;
         break;
       case '?':
         fprintf(stderr, "Error in parsing option -%c.\n",optopt);
         return 1;
      default:
         fprintf(stderr, "Error in parsing options.\n");
         abort();
    }
  }

  if(time_set==1) { 
    // Convert date with format DD/MM/YYYY to time_t, and then struct tm format.
    if(format_set==0) {
      fprintf(stderr, "Error: Time set, but format not set: "
      "use -f to specify the format for the input date.\n");
      return 1;
    }
    else { // time_set==1 and format_set==1 
      // Convert time_string to struct tm using format_string
      if(strptime(time_string,format_string,pstart)==NULL) {
        fprintf(stderr,"Error: User set date %s could not be " 
        "interpreted using user set format %s.", time_string, format_string);
        return 1;
      }
      // If date specified in command not a Monday, exit with error.
      if(pstart->tm_wday!=1) {
        fprintf(stderr,"Error: User set date %s is not a Monday.\n",
                time_string);
        return 1;
      }
    }
  }
  else if(time_set==0) {
    // If not date specified on command line use current date, and fast-forward
    // to the next Monday
    time(&rawtime);
    pnow = localtime(&rawtime);
    setstart(pnow,pstart);
  }

  printf("Diary start Time and date: %s", asctime(pstart));

  // At this point, struct tm * pstart should be initialized with a date which
  // is a Monday.

  // Open a new file, named YYYYMMDD.txt  either in current directory, or in 
  // directory specified on the command line. YYYYMMDD is the date of the
  // starting Monday. If a file of this name already exists, exit with error.
  filename = (char *) malloc(sizeof("YYYYMMDD.txt"));
  strftime(filename,sizeof("YYYYMMDD.txt"),"%Y%m%d.txt",pstart);
  if(dir_set==1) {
      // Set filepath according to whether last char in dir_string=='/'
      if(dir_string[strlen(dir_string)-1]=='/') {
        filepath = (char*) malloc((strlen(dir_string)+strlen(filename)+1)
                                  *sizeof(char));
        strcpy(filepath,dir_string);
        strcat(filepath,filename);
      }
      else {
        filepath = (char*) malloc((strlen(dir_string)+1+strlen(filename)+1)
                                  *sizeof(char));
        strcpy(filepath,dir_string);
        strcat(filepath,"/");
        strcat(filepath,filename);
      }
  }
  else { // If no directory set, assume current directory
    filepath = (char*) malloc((strlen(filename)+1)*sizeof(char));
    strcpy(filepath,filename);
  }
  // If file already exists, exit with error
  //file = fopen(filepath,"r");
  file = fopen(filepath,"r");
  if(file!=NULL) { // i.e. file exists
    fprintf(stderr,"Error: File %s exists.\n",filepath);
    fclose(file);
    return 1;
  }
  // Open new file for writing
  file = fopen(filepath,"w");
  if(file==NULL) {
    fprintf(stderr,"Error: File %s cannot be opened for writing.\n",filepath);
  }

  // Write diary template to YYYYMMDD.txt.

  max_output_size = sizeof("XX/XX/XX Wednesday");
  output_time = (char *) malloc(max_output_size);
  for(ii=0;ii<=6;ii++) {
    strftime(output_time,max_output_size,"%d/%m/%y %A",pstart);
    fprintf(file,"%s\n\n\n",output_time);
    pstart->tm_mday++;
    mktime(pstart);
  }
  time(&rawtime);
  pnow = localtime(&rawtime);
  fprintf(file,"Generated by cdiary on %s",asctime(pnow));
  // Close YYYYMMDD.txt
  fclose(file);

  printf("Diary template successfully written to %s.\n",filepath);

  // Free malloc'd memory
  free(filename);
  free(filepath);
  free(pstart);
  free(output_time);
  return 0;
}
