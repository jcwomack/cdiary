#include<stdio.h>
#include<time.h>
#include"setstart.h"

// Take in a date in struct tm form, check whether it is a Monday, if it is a
// Monday, copy the value of the struct pointed to by praw into the struct 
// pointed to by pstart. If not, make pstart point to a struct containing 
// the correct calendar date of the next Monday.
void setstart(struct tm * praw, struct tm * pstart) {
  if(praw->tm_wday==1) { 
    //printf("Today is a Monday\n"); 
    // Copy struct tm pointed to by praw to struct tm pointed by pstart
    (*pstart) = (*praw);
  }
  else { 
    //printf("Today is not a Monday\n");
    // Copy struct tm pointed to by praw to struct tm pointed by pstart
    (*pstart) = (*praw);
    // Add days to start.tm_wday and start.tm_mday to get the next Monday
    // It doesn't matter if these number go above the allowed 0..6 and 
    // 1..31 since we can use mktime to bring the struct back into the correct
    // format.
    if(praw->tm_wday<=0) {
       pstart->tm_mday += 1-praw->tm_wday;
    }
    else {
       pstart->tm_mday += 6-praw->tm_wday+2;
    }
    pstart->tm_wday = 1;
    //printf("Raw edited time and date: %s", asctime(pstart));
    // Fix any anomalies in the start struct (e.g. mday > 31).
    mktime(pstart);
  };
}
